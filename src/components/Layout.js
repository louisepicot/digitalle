import React, { useEffect, useState } from 'react';
import 'normalize.css';
import GlobalStyles from '../styles/GlobalStyles';
import Typography from '../styles/Typography';
import Grid from './Grid';
import SEO from './SEO';
import Page404Query from './Page404';

export default function Layout({ children }) {
  return (
    <>
      <SEO
        description={children.props.data?.seo?.description}
        image={children.props.data?.seo?.image?.asset?.fluid?.src}
        title={children.props.data?.seo?.title}
        titlePage={children.props.data?.seo?.titlePage}
        location={children.props.location}
      />
      <GlobalStyles page={children.props.location.pathname} />
      <Typography />
      {!children.props.custom404 ? (
        <Grid dataPage={children} />
      ) : (
        <Page404Query dataPage={children} />
      )}
    </>
  );
}
