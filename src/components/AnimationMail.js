import React from 'react';
import AnimationMailStyle from '../styles/AnimationMailStyle';
import contactUs from '../assets/images/contactus.png';

export default function AnimationMail({}) {
  return (
    <a href="mailto:hello@digitalle.ch">
      <AnimationMailStyle>
        <img src={contactUs} alt="" />
      </AnimationMailStyle>

      {/* <AnimationMailStyle>
        <div id="circle">
          <svg
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
            xmlnslink="http://www.w3.org/1999/xlink"
            x="0px"
            y="0px"
            width="200px"
            height="200px"
            viewBox="0 0 300 300"
            enableBackground="new 0 0 300 300"
            xmlSpace="preserve"
          >
            <defs>
              <path
                id="circlePath"
                d=" M 150, 150 m -60, 0 a 60,60 0 0,1 120,0 a 60,60 0 0,1 -120,0 "
              />
            </defs>
            <circle cx="150" cy="100" r="75" fill="none" />
            <g>
              <use xlinkHref="#circlePath" fill="none" />
              <text fill="#000">
                <textPath xlinkHref="#circlePath">Contact Us !</textPath>
              </text>
              <text fill="#000" className="doodle">
                <textPath xlinkHref="#circlePath">Contact Us 3</textPath>
              </text>
            </g>
          </svg>
        </div>
      </AnimationMailStyle> */}
    </a>
  );
}
