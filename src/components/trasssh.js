<fieldset>
  <label htmlFor="first-name">
    <h5>First name</h5>
    <input
      type="text"
      name="first-name"
      id="first-name"
      value={values.firstName}
      onChange={updateValue}
    />
  </label>
  <label htmlFor="last-name">
    <h5>Last name</h5>
    <input
      type="text"
      name="last-name"
      id="last-name"
      value={values.lastName}
      onChange={updateValue}
    />
  </label>
  <label htmlFor="email">
    <h5>Email</h5>
    <input
      type="email"
      name="email"
      id="email"
      value={values.email}
      onChange={updateValue}
    />
  </label>

  <div className="button">
    <div aria-live="polite" aria-atomic="true">
      {/* {error ? <p>Error: {error}</p> : ''} */}
    </div>
    <button type="submit">
      <h5>Subscribe</h5>
      <span aria-live="assertive" aria-atomic="true">
        {/* {loading ? 'Placing Order...' : ''} */}
      </span>
      {/* {loading ? '' : 'Order Ahead'} */}
    </button>
  </div>
</fieldset>;
