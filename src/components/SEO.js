import React from 'react';
import { Helmet } from 'react-helmet';
import { useStaticQuery, graphql } from 'gatsby';

export default function SEO({
  location,
  description,
  title,
  image,
  titlePage,
}) {
  const { site } = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
          description
        }
      }
    }
  `);
  return (
    <Helmet titleTemplate={`%s ${site.siteMetadata.title}`}>
      <html lang="en" />
      <title>{title || ' '}</title>
      {/* Fav Icons */}

      {/* <link
        rel="apple-touch-icon"
        sizes="180x180"
        href="../assets/favicon_io/apple-touch-icon.png"
      />
      <link
        rel="icon"
        type="image/png"
        sizes="32x32"
        href="../assets/favicon_io/favicon-32x32.png"
      />
      <link
        rel="icon"
        type="image/png"
        sizes="16x16"
        href="/favicon-16x16.png"
      />
      <link rel="manifest" href="../assets/favicon_io/site.webmanifest" />

      <link rel="alternate icon" href="../assets/favicon_io/favicon.ico" /> */}
      {/* Meta Tags */}
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta charSet="utf-8" />
      <meta
        name="description"
        content={description || site.siteMetadata.description}
      />

      {/* Open Graph */}
      {location && <meta property="og:url" content={location.href} />}

      <meta
        property="og:image"
        content={image || '../assets/images/contactus.png'}
      />
      <meta
        property="og:title"
        content={titlePage || site.siteMetadata.title}
        key="ogtitle"
      />
      <meta
        propery="og:site_name"
        content={site.siteMetadata.title}
        key="ogsitename"
      />
      <meta
        property="og:description"
        content={description || site.siteMetadata.description}
        key="ogdesc"
      />
    </Helmet>
  );
}
