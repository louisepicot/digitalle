import React from 'react';
import StatementStyle from '../styles/StatementStyle';

const Statement = ({ index }) => (
  <StatementStyle className={`home${index} texte-div`}>
    <h3>
      <span className={`${index === 0 && 'show-h doodle'} doodle span-0`}>
        0
      </span>
      <span className={`${index === 1 && 'show-h'} span-1`}>
        ... is a culture; it starts and ends with people.{' '}
      </span>

      <span className={`${index === 2 && 'show-h'} span-2`}>
        Principles not models: the roots of success are based on curiosity,
        serendipity & unpredictable outcomes.{' '}
      </span>
      <span className={`${index === 3 && 'show-h doodle'} doodle span-3`}>
        2
      </span>

      <span className={`${index === 4 && 'show-h'} span-4`}>
        Leadership is liquid and built on inclusion: be nimble and free to
        question anything.{' '}
      </span>
      <span className={`${index === 5 && 'show-h doodle'} doodle span-5`}>
        1
      </span>

      <span className={`${index === 6 && 'show-h'} span-6`}>
        Teaching is learning; Help is here if you want it…
      </span>
      <span className={`${index === 7 && 'show-h doodle'} doodle span-7`}>
        3
      </span>
    </h3>
  </StatementStyle>
);

export default Statement;
