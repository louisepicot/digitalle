import React from 'react';

import SanityBlockContent from '@sanity/block-content-to-react';
import Img from 'gatsby-image';
import IntroStyle from '../styles/IntroStyle';

export default function Intro({ dataIntro }) {
  return (
    <IntroStyle>
      <div className="box-text">
        {dataIntro.illu && <Img fluid={dataIntro.illu.asset.fluid} alt="" />}
        {dataIntro._rawText && (
          <SanityBlockContent
            blocks={dataIntro._rawText}
            projectId="009ov7e5"
            dataset="production"
          />
        )}
      </div>
    </IntroStyle>
  );
}
