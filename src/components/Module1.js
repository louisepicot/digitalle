import React from "react";

import SanityBlockContent from "@sanity/block-content-to-react";
import Img from "gatsby-image";
import { ItemsStyle, Module1Style } from "../styles/Module1Style";

export default function Module1({ data }) {
  return (
    <Module1Style>
      <ItemsStyle>
        <h2>
          {data.illu && <Img fluid={data.illu.asset.fluid} alt="hello" />}
          {data.title}
        </h2>
      </ItemsStyle>
      <ItemsStyle className="text-1">
        {data._rawText && (
          <SanityBlockContent
            blocks={data._rawText}
            projectId="009ov7e5"
            dataset="production"
          />
        )}
      </ItemsStyle>
    </Module1Style>
  );
}
