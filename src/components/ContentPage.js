import React, { useState, useEffect, useRef } from 'react';

import { InView } from 'react-intersection-observer';
import { polyfill } from 'seamless-scroll-polyfill';
import smoothscroll from 'smoothscroll-polyfill';
import Intro from './Intro';
import Module1 from './Module1';
import Module2 from './Module2';
import Navbar from './Navbar';
import slugify from '../utils/slugify';
import FormNewsletter from './FormNewsletter';
import Footer from './Footer';
import ContentPageStyle from '../styles/ContentPageStyle';
import AnimationMail from './AnimationMail';

const BlockContentFilter = ({ dataObj, currentY, setEntry }) => (
  <>
    {dataObj.moduleType.moduleType === 'module1' && (
      <InView
        as="div"
        threshold={0.2}
        onChange={(inView, entry) => inView && setEntry(entry.target)}
        className={`${slugify(dataObj.category)} snap-scroll`}
      >
        <Module1 data={dataObj.moduleType.module1} />
      </InView>
    )}

    {dataObj.moduleType.moduleType === 'module2' && (
      <InView
        threshold={0}
        as="div"
        // onChange={(inView, entry) => inView && setEntry(entry.target)}
        className={`${slugify(dataObj.category)} snap-scroll modul2-snap`}
      >
        <Module2 data={dataObj.moduleType.module2} currentY={currentY} />
      </InView>
    )}
  </>
);

export default function ContentPage({ data, titles, page }) {
  const prevScrollY = useRef(0);
  const [goingUp, setGoingUp] = useState(false);
  const [currentY, setCurrentY] = useState(0);
  const [currentEntry, setCurrentEntry] = useState('');

  const onScroll = (e) => {
    const currentScrollY = e.target.scrollTop;
    if (prevScrollY.current < currentScrollY && goingUp) {
      setGoingUp(false);
    }
    if (prevScrollY.current > currentScrollY && !goingUp) {
      setGoingUp(true);
    }

    prevScrollY.current = currentScrollY;
    setCurrentY(currentScrollY);
  };

  const [isDesktop, setDesktop] = useState(false);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      setDesktop(window.innerWidth > 768);
    }
  }, []);

  const updateMedia = () => {
    setDesktop(window.innerWidth > 768);
  };

  useEffect(() => {
    if (isDesktop) {
      if (
        currentEntry &&
        !currentEntry.classList.contains('modul2-snap') &&
        !currentEntry.classList.contains('intro-snap')
      ) {
        polyfill();
        smoothscroll.polyfill();
        // currentEntry.scrollIntoView();
        currentEntry.scrollIntoView({
          behavior: 'smooth',
          block: 'end',
        });
      }
    }

    window.addEventListener('resize', updateMedia);
    return () => window.removeEventListener('resize', updateMedia);
  }, [currentEntry, isDesktop]);

  return (
    <ContentPageStyle
      className="scrollPart"
      onScroll={onScroll}
      goingUp={goingUp}
    >
      <Navbar
        titles={titles}
        page={page}
        currentEntry={currentEntry}
        isDesktop={isDesktop}
      />
      {data.map(
        (dataObj) =>
          dataObj.intro && (
            <InView
              key={dataObj.id}
              threshold={0.2}
              as="div"
              onChange={(inView, entry) =>
                inView && setCurrentEntry(entry.target)
              }
              className={`${slugify(dataObj.category)} intro-snap snap-scroll`}
            >
              <>
                <Intro dataIntro={dataObj.intro} />
              </>
            </InView>
          )
      )}
      {data.map(
        (dataObj, index) =>
          !dataObj.intro &&
          dataObj.category && (
            <BlockContentFilter
              dataObj={dataObj}
              currentY={currentY}
              key={dataObj.id}
              setEntry={setCurrentEntry}
            />
          )
      )}
      {page === 'About' && (
        <InView
          threshold={0.2}
          as="div"
          onChange={(inView, entry) => inView && setCurrentEntry(entry.target)}
          className="newsletter snap-scroll"
        >
          <FormNewsletter />
        </InView>
      )}
      <AnimationMail />
      <Footer />
    </ContentPageStyle>
  );
}
