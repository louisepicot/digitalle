import React from 'react';
import { H5Style, NavbarStyle } from '../styles/NavbarStyle';

import slugify from '../utils/slugify';

export default function Navbar({ titles, page, currentEntry, isDesktop }) {
  const entry = currentEntry ? currentEntry.classList[0] : '';

  // const [indexShow, setIndexShow] = useState(false);

  const handleClick = (title) => {
    if (document.querySelector(`.${title}`)) {
      if (
        document
          .querySelectorAll(`.${title}`)[0]
          .classList.contains('modul2-snap')
      ) {
        document
          .querySelectorAll(`.${title}`)[0]
          .scrollIntoView(isDesktop && { block: 'start' });
      } else {
        document
          .querySelectorAll(`.${title}`)[0]
          .scrollIntoView(isDesktop && { block: 'end' });
      }
    }
  };

  return (
    <>
      <h2 className="hide-mobil">{page}</h2>
      <NavbarStyle className="navbar">
        {titles.map((title, index) => (
          <H5Style
            className={entry === slugify(title) ? 'show' : ''}
            onClick={(e) => handleClick(slugify(title))}
            key={title}
            indexLast={index === titles.length - 1}
            indexFirst={index === 0}
          >
            {title}
          </H5Style>
        ))}
      </NavbarStyle>
    </>
  );
}
