import React, { useState, useEffect, useRef } from 'react';

import SanityBlockContent from '@sanity/block-content-to-react';
import Img from 'gatsby-image';
import { useInView } from 'react-intersection-observer';
import { Module2Style, Tiroir } from '../styles/Module2Style';

// import _ from 'lodash';
// import sal from 'sal.js';

export default function Module2({ data, currentY }) {
  const [numTiroir, setNumTiroir] = useState(0);
  const { ref, inView, entry } = useInView({
    /* Optional options */
    threshold: 0,
  });

  useEffect(() => {
    if (inView) {
      const arrayPoint = [
        entry.target.offsetTop,
        entry.target.offsetTop + 500,
        entry.target.offsetTop + 1000,
        entry.target.offsetTop + 1500,
        entry.target.offsetTop + 2000,
      ];

      // if(prevScrollY + )
      arrayPoint.forEach((point, index) => {
        if (currentY >= point && currentY <= arrayPoint[index + 1]) {
          setNumTiroir(index);
        }
      });
    }

    // return () => {
    //   second
    // }
  }, [inView, currentY]);

  return (
    <Module2Style ref={ref}>
      <div className="flex-box-align-end sticky">
        {data.tiroir.map((elem, index) => (
          <Tiroir key={elem._key} open={index === numTiroir} className="tiroir">
            {elem.illu && <Img fluid={elem.illu.asset.fluid} alt="hello" />}
            <div className="show">
              {elem._rawText && (
                <SanityBlockContent
                  blocks={elem._rawText}
                  projectId="009ov7e5"
                  dataset="production"
                />
              )}
            </div>
          </Tiroir>
        ))}
      </div>
    </Module2Style>
  );
}
