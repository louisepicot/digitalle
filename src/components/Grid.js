import React from 'react';
import { Link } from 'gatsby';
import { GridStyle, ItemStyle } from '../styles/GridStyle';

export default function Grid({ dataPage }) {
  const urlPath = dataPage.props.location.pathname;

  return (
    <GridStyle colorTheme={urlPath}>
      <ItemStyle
        className="items home"
        show={urlPath === '/'}
        urlPath={urlPath === '/'}
      >
        <Link to="/" className="link-item">
          <div className="hover">
            <span className="logo">
              <h5>Digitalle</h5>
            </span>
          </div>
        </Link>
        {(urlPath === '/' || dataPage.props.custom404) && dataPage}
      </ItemStyle>
      <ItemStyle
        className="items about"
        show={urlPath === '/about' || urlPath === '/about/'}
      >
        <Link to="/about" className="link-item">
          <div className="hover">
            <span>
              <h5>about</h5>
            </span>
          </div>
        </Link>
        {(urlPath === '/about' || urlPath === '/about/') && dataPage}
      </ItemStyle>
      <ItemStyle
        className="items services"
        show={urlPath === '/services' || urlPath === '/services/'}
      >
        <Link to="/services" className="link-item">
          <div className="hover">
            <span>
              <h5>services</h5>
            </span>
          </div>
        </Link>
        {(urlPath === '/services' || urlPath === '/services/') && dataPage}
      </ItemStyle>
      <ItemStyle
        className="items workshops"
        show={urlPath === '/workshops' || urlPath === '/workshops/'}
      >
        <Link to="/workshops" className="link-item">
          <div className="hover">
            <span>
              <h5>workshops</h5>
            </span>
          </div>
        </Link>
        {(urlPath === '/workshops' || urlPath === '/workshops/') && dataPage}
      </ItemStyle>
    </GridStyle>
  );
}
