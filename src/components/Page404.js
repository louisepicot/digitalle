import React from 'react';
import { Link, StaticQuery, graphql } from 'gatsby';

import Page404Style from '../styles/Page404Style';
import { GridStyle, ItemStyle } from '../styles/GridStyle';

import Footer from './Footer';

const Page404 = ({ data }) => {
  console.log(data, 'hu');
  return (
    <GridStyle colorTheme="404">
      <ItemStyle className="items home" show urlPath>
        <Link to="/" className="link-item">
          <div className="hover">
            <span className="logo">
              <h5>Digitalle</h5>
            </span>
          </div>
        </Link>
        <Page404Style>
          <div className="content">
            <h5>{data.sanityPage404.title}</h5>
          </div>
          <Footer />
        </Page404Style>
      </ItemStyle>
      <ItemStyle className="items about">
        <Link to="/about" className="link-item">
          <div className="hover">
            <span>
              <h5>about</h5>
            </span>
          </div>
        </Link>
      </ItemStyle>
      <ItemStyle className="items services">
        <Link to="/services" className="link-item">
          <div className="hover">
            <span>
              <h5>services</h5>
            </span>
          </div>
        </Link>
      </ItemStyle>
      <ItemStyle className="items workshops">
        <Link to="/workshops" className="link-item">
          <div className="hover">
            <span>
              <h5>workshops</h5>
            </span>
          </div>
        </Link>
      </ItemStyle>
    </GridStyle>
  );
};

export default function Page404Query(props) {
  return (
    <StaticQuery
      query={graphql`
        query {
          sanityPage404 {
            _rawText
            title
          }
        }
      `}
      render={(data) => <Page404 data={data} {...props} />}
    />
  );
}
