import React from 'react';

import { Link } from 'gatsby';
import instagram from '../assets/images/instagram.png';
import linkedin from '../assets/images/linkedin.png';
import FooterStyle from '../styles/FooterStyle';

export default function Footer({}) {
  return (
    <FooterStyle className="snap-scroll">
      <div className="row1">
        <div className="item">
          <h4 className="logo">Digitalle</h4>
          <p>Stop "doing digital" and start "being digitalle".</p>
        </div>
        <div className="item">
          <a href="mailto: info@digitalle.ch">
            <h4>HAVE QUESTIONS? WE GOT ANSWERS</h4>
          </a>
          <h4>+41796208172</h4>
          <div className="icon-list">
            <h4>FOLLOW US ON :</h4>
            <a href="https://www.instagram.com/digitalle.ch/">
              <img className="icon" src={instagram} alt="blablabla" />
            </a>
            <a href="https://www.linkedin.com/company/77016912/">
              <img className="icon" src={linkedin} alt="blablabla" />
            </a>
          </div>
        </div>
        <div className="item">
          <Link to="/about">
            <h4>ABOUT</h4>
          </Link>
          <Link to="/services">
            <h4>SERVICES</h4>
          </Link>
          <Link to="/workshops">
            <h4>WORKSHOPS</h4>
          </Link>
        </div>
      </div>
      <div className="row2">
        {' '}
        <div className="item">
          <h4>BRANDING</h4>
          <p>© 2021. Chat & Sa.</p>
        </div>
        <div className="item">
          <h4>INTERFACE DESIGN & DEVELOPMENT</h4>
          <p>© 2022. Une Place au Soleil.</p>
        </div>
        <div className="item">
          <h4>TERMS & CONDITIONS</h4>
          <h4>PRIVACY POLICY</h4>
        </div>
      </div>
    </FooterStyle>
  );
}
