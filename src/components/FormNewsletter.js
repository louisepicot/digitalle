import React from "react";
import Mailchimp from "react-mailchimp-form";
import FormStyle from "../styles/FormStyle";

export default function FormNewsletter() {
  return (
    <FormStyle>
      <h3>
        Subscribe to our bi-monthly newsletter in which we explore the realities
        of digital | work.
      </h3>
      <h3>
        It’s about how we live online: what we’re uploading, downloading,
        streaming, liking, swiping, and otherwise obsessing over this moment.
      </h3>
      <Mailchimp
        action="https://digitalle.us1.list-manage.com/subscribe/post?u=0d6a51fc571f5eff76ed419b4&amp;id=d1cd053980"
        fields={[
          {
            name: "FNAME",
            placeholder: "Last name",
            type: "text",
            required: true,
          },
          {
            name: "LNAME",
            placeholder: "First name",
            type: "text",
            required: true,
          },
          {
            name: "EMAIL",
            placeholder: "Email",
            type: "email",
            required: true,
          },
        ]}
        // Change predetermined language
        messages={{
          sending: "Sending...",
          success: "Thank you for subscribing!",
          error: "An unexpected internal error has occurred.",
          empty: "You must write an e-mail.",
          duplicate: "Too many subscribe attempts for this email address",
          button: "subscribe",
        }}
      />
    </FormStyle>
  );
}
