import styled from 'styled-components';

export const Module2Style = styled.div`
  position: relative;
  overflow: scroll;
  top: 0px;
  height: auto;

  a {
    font-weight: bold !important;
    text-decoration: underline !important;
  }
  .flex-box-align-end {
    height: auto;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    -webkit-justify-content: space-between;
    /* overscroll-behavior-y: contain; */
  }

  @media (min-width: 900px) {
    margin-top: 10vh;
    overflow: visible;
    height: 300vh;
    .sticky {
      position: sticky;
      top: 23vh;
    }
    .flex-box-align-end {
      height: 71.3vh;
      width: calc(100% - 100px);
      margin-right: 100px;
      display: flex;

      flex-direction: row;
      justify-content: end;
      -webkit-justify-content: flex-end;
      /* overscroll-behavior-y: contain; */
    }
  }
`;

export const Tiroir = styled.div`
  overflow: scroll;
  transition: all 0.25s;
  margin-top: 60px;
  :first-child {
    margin-top: 0px;
  }

  .gatsby-image-wrapper {
    max-width: 30vw;
    width: 30vw;
    margin: 0 auto;
    height: fit-content !important;
    object-fit: contain !important;
  }

  .image-center-responsive {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  @media (min-width: 900px) {
    margin-top: 0;
    .gatsby-image-wrapper {
      width: 30vh;
      float: right;
      height: fit-content !important;

      object-fit: contain !important;
    }
    padding: 20px;
    padding: 0px 20px;

    display: flex;

    justify-content: space-between;
    -webkit-justify-content: space-between;
    transition: all 0.25s;
    width: ${(props) => (props.open ? '100%' : '160px')};
    border-left: ${(props) =>
      props.index === 0 ? '' : '1px solid var(--font-color)'};
    overflow: hidden;
    .show {
      padding: 0px 30px;
      display: ${(props) => (props.open ? 'flex' : 'none')};
      width: 500px;
      padding: 0px 30px;
      flex-basis: revert;
      align-items: flex-end;
      -webkit-align-items: flex-end;
      h4 {
        font-size: 2.5rem;
        line-height: 3.5rem;
      }
      h4,
      p {
        margin-left: 0;
        margin-right: 0;
      }
    }
  }
`;
