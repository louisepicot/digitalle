import styled from 'styled-components';

const IntroStyle = styled.div`
  display: flex;
  justify-content: end;

  -webkit-justify-content: flex-end;
  flex-direction: column;
  position: relative;

  p {
    font-weight: normal;
    margin: 0;
    font-family: recoletaAlt, -apple-system, BlinkMacSystemFont, 'Segoe UI',
      Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue',
      sans-serif;
    font-size: 2.5rem;
    line-height: 3rem;
  }

  .gatsby-image-wrapper {
    width: 100%;
  }

  @media (min-width: 900px) {
    .box-text {
      height: 100%;
      display: flex;
      align-items: flex-end;
    }

    height: 71vh;
    padding-top: 85px;
    .gatsby-image-wrapper {
      float: right;
      max-width: 25vw;
      width: 25vw;
      position: absolute !important;
      right: 0px;
      top: 0px;
    }

    p {
      font-size: 5rem;
      line-height: 6rem;
    }
  }

  @media (min-width: 1600px) {
    height: 73vh;
    padding-top: 10vh;
  }
`;

export default IntroStyle;
