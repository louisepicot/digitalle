import styled from 'styled-components';

const HomePageStyle = styled.div`
  /* width: 100%;
  padding: 0px 50px 0px 50px; */
  position: relative;
  overflow-y: scroll;
  height: calc(78vh - 20px);
  width: auto;
  padding: 0px 70px 0px 0px;

  .texte-div {
    position: sticky;
    top: 17%;
    margin-top: 10vh;
  }
  .content {
    height: 600vh;
    position: relative;
    display: block;
    margin-bottom: 20px;
  }
  span {
    opacity: 0;
    position: relative;
    margin-top: 20px;
  }
  .doodle {
    font-family: doodle;
    font-size: 14rem;
    line-height: 0;
    line-height: auto;
    color: black;
  }

  .span-1,
  .span-4 {
    /* text-align: right; */
    color: #feb09a;
  }

  .span-3 {
    padding-left: 30px;
  }
  .span-0 {
    padding-top: 20px;
  }
  .show-h {
    opacity: 1 !important;
  }

  @media (min-width: 900px) {
    .texte-div {
      position: sticky;
      top: 18%;
      margin-top: 20vh;
    }
    overflow-y: scroll;
    height: calc(100vh - 20px);

    .content {
      height: 1000vh;
      position: relative;
      display: block;
      /* margin-bottom: 10vh; */
    }

    span {
      opacity: 0;
      position: relative;
      font-weight: normal;
      margin: 0;
      font-family: recoletaAlt, -apple-system, BlinkMacSystemFont, 'Segoe UI',
        Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue',
        sans-serif;
    }
  }

  @media (min-width: 1300px) {
    .texte-div {
      position: sticky;
      top: 28%;
      margin-top: 40vh;
    }
    .content {
      height: 800vh;
      position: relative;
      display: block;
      /* margin-bottom: 10vh; */
    }
  }

  @media (max-width: 900px) {
    .content {
      height: 700vh;
      position: relative;
      display: block;
      padding: 20px;
    }
    padding: 9vh 20px;

    .doodle {
      font-family: doodle;
      font-size: 7rem;
      line-height: 0;
      line-height: auto;
      color: black;
    }
  }
`;

export default HomePageStyle;
