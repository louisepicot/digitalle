import styled from 'styled-components';

export const NavbarStyle = styled.div`
  display: flex;
  background-color: var(--background-color);
  justify-content: space-between;
  -webkit-justify-content: space-between;
  position: sticky;
  border-top: 1px solid var(--font-color);
  top: 0;
  z-index: 11111;
  flex-direction: row-reverse;
  margin-right: 20px;
  margin-left: 20px;

  .show {
    /* padding-top: 1rem; */
    display: block;
    background-color: var(--background-color);
    border-bottom: 1px solid;
  }

  .show ~ h5 {
    border-top: 1px solid;
    /* padding-top: 17px; */
  }

  @media (min-width: 900px) {
    margin-right: 0px;
    margin-left: 0px;
    .show {
      border-top: 4px solid;
      padding-top: 17px;
      background-color: transparent;
      border-bottom: 0;
    }

    .show ~ h5 {
      border-top: 4px solid;
      padding-top: 17px;
    }
  }
  @media (max-width: 900px) {
    display: none;
  }
`;

export const H5Style = styled.h5`
  width: 100%;
  display: none;
  text-align: center;

  cursor: pointer;

  @media (min-width: 900px) {
    padding-left: 0px;
    margin-right: 0px;
    text-align: left;
    margin-left: 0px;
    padding-top: 20px;
    display: block;
    text-align: ${(props) => {
      if (props.indexFirst) {
        return 'right';
      }
      if (props.indexLast) {
        return 'left';
      }
      return 'center';
    }};
  }
`;
