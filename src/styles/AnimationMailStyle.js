import styled from 'styled-components';

const AnimationMailStyle = styled.div`
  cursor: pointer;
  #circle text {
    font-family: founderNormal, -apple-system, BlinkMacSystemFont, 'Segoe UI',
      Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue',
      sans-serif;
    text-transform: uppercase;
    font-size: 5.25rem;
  }
  .doodle {
    font-family: doodle !important;
    color: black;
  }
  img {
    z-index: 9000000;

    filter: inherit;
    position: fixed;
    top: 2rem;
    right: 2rem;
    width: 7vw;
    height: 7vw;
    -webkit-animation-name: rotate;
    -moz-animation-name: rotate;
    -ms-animation-name: rotate;
    -o-animation-name: rotate;
    animation-name: rotate;
    -webkit-animation-duration: 5s;
    -moz-animation-duration: 5s;
    -ms-animation-duration: 5s;
    -o-animation-duration: 5s;
    animation-duration: 5s;
    -webkit-animation-iteration-count: infinite;
    -moz-animation-iteration-count: infinite;
    -ms-animation-iteration-count: infinite;
    -o-animation-iteration-count: infinite;
    animation-iteration-count: infinite;
    -webkit-animation-timing-function: linear;
    -moz-animation-timing-function: linear;
    -ms-animation-timing-function: linear;
    -o-animation-timing-function: linear;
    animation-timing-function: linear;
  }

  @-webkit-keyframes rotate {
    from {
      -webkit-transform: rotate(360deg);
    }
    to {
      -webkit-transform: rotate(0);
    }
  }
  @-moz-keyframes rotate {
    from {
      -moz-transform: rotate(360deg);
    }
    to {
      -moz-transform: rotate(0);
    }
  }
  @-ms-keyframes rotate {
    from {
      -ms-transform: rotate(360deg);
    }
    to {
      -ms-transform: rotate(0);
    }
  }
  @-o-keyframes rotate {
    from {
      -o-transform: rotate(360deg);
    }
    to {
      -o-transform: rotate(0);
    }
  }
  @keyframes rotate {
    from {
      transform: rotate(360deg);
    }
    to {
      transform: rotate(0);
    }
  }
  @media (min-width: 900px) {
    #circle text {
      font-size: 3.35rem;
    }
    #circle svg {
      top: -10px;
      right: -10px;
      width: 20vh;
      height: 20vh;
    }
  }
  @media (max-width: 900px) {
    img {
      top: 70vh;
      right: 5%;
      width: 12vh;
      height: 12vh;
    }
  }
`;

export default AnimationMailStyle;
