import { createGlobalStyle } from 'styled-components';
import stripes from '../assets/images/stripes.svg';

const GlobalStyles = createGlobalStyle`


  :root {
    --red: #FF4949;
    --black: #2E2E2E;
    --yellow: #ffc600;
    --white: #fff;
    --grey: #efefef;
    --font-size: 1vh;
    --background-color: ${(props) => {
      if (props.page === '/about' || props.page === '/about/') {
        return 'rgb(194, 226, 217)';
      }
      if (props.page === '/services' || props.page === '/services/') {
        return 'rgb(233, 229, 218)';
      }
      if (props.page === '/workshops' || props.page === '/workshops/') {
        return 'rgb(240, 226, 217)';
      }
      if (props.page === '/') {
        return 'rgb(250, 248, 246)';
      }
    }};
    --font-color: ${(props) => {
      if (props.page === '/about' || props.page === '/about/') {
        return 'rgb(224, 94, 41) !important';
      }
      if (props.page === '/services' || props.page === '/services/') {
        return 'rgb(73, 88, 48) !important';
      }
      if (props.page === '/workshops' || props.page === '/workshops/') {
        return 'rgb(210, 70, 79) !important';
      }
      if (props.page === '/') {
        return 'rgb(50, 126, 97) !important';
      }
      return 'black';
    }}
  }








  html {
    /* background-size: 450px;
    background-attachment: fixed; */
    font-size: var(--font-size);

  }

  body {
    font-size: 2rem;

  }

  fieldset {
    border-color: rgba(0,0,0,0.1);
    border-width: 1px;
  }

  p {
    padding: 0;
    margin: 0;
  }

  a {
    text-decoration: none;
    color: var(--font-color);
  }


  .gatsby-image-wrapper img[src*=base64] {
  }

  /* Scrollbar Styles */
  body::-webkit-scrollbar {
    width: 4px;
    scrollbar-color: var(----font-color) var(--background-color);
  }
  html {
    scrollbar-width: thin;
    scrollbar-color: var(----font-color) var(--background-color);
  }
  body::-webkit-scrollbar-track {
    background: var(---background-color);
  }
  body::-webkit-scrollbar-thumb {
    background-color: var(----font-color) ;
    border-radius: 6px;
    border: 3px solid var(----font-color);
  }

  /* .scrollPart::-webkit-scrollbar {
    width: 4px;
    scrollbar-color: var(----font-color) var(--background-color);
  }
  .scrollPart {
    scrollbar-width: thin;
    scrollbar-color: var(----font-color) var(--background-color);
  }

  .scrollPart::-webkit-scrollbar-track {
    background: var(---background-color);
  }

  .scrollPart::-webkit-scrollbar-thumb {
    background-color: var(----font-color) ;
    border-radius: 6px;
    border: 3px solid var(----font-color);
  } */

  hr {
    border: 0;
    height: 8px;
    background-image: url(${stripes});
    background-size: 1500px;
  }

  img {
    max-width: 100%;

  }

  .hide-mobil{
    display: none;
  }
  @media (min-width: 900px) {
    .hide-mobil {
      display: block;
    }
  }
`;

export default GlobalStyles;
