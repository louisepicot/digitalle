import styled from 'styled-components';

export const Module1Style = styled.div`
  .gatsby-image-wrapper {
    max-width: 30vw;
    width: 30vw;
    margin: 0 auto;
  }
  a {
    font-weight: bold !important;
    text-decoration: underline !important;
  }
  @media (min-width: 900px) {
    min-height: 100vh;
    /* margin-top: 10vh; */
    .gatsby-image-wrapper {
      max-width: inherit;
      float: right;
    }
    display: flex;
    height: 80vh;
    justify-content: space-between;
    -webkit-justify-content: space-between;
  }
`;

export const ItemsStyle = styled.div`
  position: relative;
  margin-top: 0px;
  h2 {
    /* line-height: normal; */
    margin-bottom: 20px;
    text-align: center;
  }

  h4 {
    text-align: left;
  }

  @media (min-width: 900px) {
    h2 {
      margin-bottom: -10px;
      text-align: left;
    }
    display: flex;
    align-items: flex-end;
    -webkit-align-items: flex-end;

    width: calc(50% - 25px);
    .gatsby-image-wrapper {
      position: absolute !important;
      top: 38%;
      right: 0%;
      width: 60%;
    }
  }
`;
