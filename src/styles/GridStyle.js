import styled from 'styled-components';

export const ItemStyle = styled.div`
  width: 100%;
  transition: width 0.15s ease-in-out 0s;

  .link-item {
    h5 {
      padding: 3px 0 0px 20px;
      border-bottom: 0px;
      border-top: 1px solid;
    }
  }

  .logo {
    /* color: black; */
    h5 {
      font-family: RecoletaMedium;
      text-transform: none;
      font-size: 2.5rem;
    }
  }

  @media (max-width: 900px) {
    width: 100%;

    background-color: var(--background-color);
    .logo h5 {
      font-size: 4rem;
    }

    .link-item {
      z-index: 1222222222200;
      position: fixed;
      z-index: 1000;
      height: 3vh;
      background-color: var(--background-color);
      /* display: ${(props) => (props.show ? 'none' : 'block')}; */
      h5 {
        padding: 5px 0;
        border-top: 0px solid;

        background-color: var(--background-color);
      }
    }
    &&.home {
      .link-item {
        left: 0vw;
        width: 100%;
        top: 0px;
        h5 {
          padding-left: 20px;
        }
      }
    }

    &&.about {
      .link-item {
        width: 100%;
        top: 6.5vh;

        left: 0vw;
        h5 {
          padding-left: 20px;
          border-bottom: 1px solid;
        }
      }
    }
    &&.services {
      .link-item {
        width: 100%;
        top: 6.5vh;
        left: calc(75vw / 2);
        h5 {
          border-bottom: 1px solid;
        }
      }
    }
    &&.workshops {
      .link-item {
        top: 6.5vh;

        right: 0vw;
        text-align: left;
        h5 {
          padding-right: 20px;
          border-bottom: 1px solid;
        }
      }
    }
  }

  @media (min-width: 900px) {
    width: ${(props) => (props.show ? 'calc(100% - 300px)' : '4%')};
    flex-grow: ${(props) => (props.show ? '1' : '0')};
    border-left: 1px solid var(--font-color);
    display: flex;

    .intro-snap.snap-scroll {
      padding-bottom: 0 !important;
    }

    max-height: 100%;
    .hover {
      height: 100%;
      width: 50px;
      transition: width 0.15s ease-in-out 0s, height 0.15s ease-in-out 0s;
    }
    &:hover {
      width: ${(props) => (props.show ? 'calc(100% - 300px)' : '5%')};
    }

    .logo {
      h5 {
        font-family: RecoletaMedium;
        text-transform: none;
        font-size: ${(props) => (!props.urlPath ? '3.5rem' : '7rem')};
      }
    }

    .link-item {
      position: relative;
      /* width: 100%; */
      color: var(--font-color);
      span {
        transition: all 0.3s ease-in 0.1s;
        display: flex;
        justify-content: ${(props) => (!props.urlPath ? 'flex-end' : '')};
        transform: ${(props) => (!props.urlPath ? 'rotate(90deg)' : '')};
        position: ${(props) => (!props.urlPath ? 'absolute' : '')};

        bottom: 10px;
        transform-origin: right bottom;
        width: 20px;
        padding-bottom: 10px;
        left: -20px;
      }

      span.logo {
        position: relative;
        bottom: 10px;
        justify-content: baseline;
        width: 1px;
        padding-bottom: 3px;
        left: 0;

        padding-top: ${(props) => (!props.urlPath ? '9rem' : '20px')};
      }
      h5 {
        padding: 10px 0 10px 20px;
        border-bottom: 0;
        border-top: 0;
      }
    }
  }
`;

export const GridStyle = styled.div`
  /* gap: 20px; */
  padding: 0px 0;
  /* height: 100vh; */
  width: 100vw;
  background-color: var(--background-color);
  color: var(--font-color);

  img {
    filter: ${(props) => {
      if (props.colorTheme === '/about' || props.colorTheme === '/about/') {
        return 'invert(42%) sepia(43%) saturate(2282%) hue-rotate(348deg) brightness(94%) contrast(86%);';
      }
      if (
        props.colorTheme === '/services' ||
        props.colorTheme === '/services/'
      ) {
        return 'invert(27%) sepia(45%) saturate(392%) hue-rotate(41deg) brightness(102%) contrast(91%);';
      }
      if (
        props.colorTheme === '/workshops' ||
        props.colorTheme === '/workshops/'
      ) {
        return 'invert(39%) sepia(36%) saturate(2374%) hue-rotate(324deg) brightness(86%) contrast(90%)';
      }
      if (props.colorTheme === '/') {
        return 'invert(44%) sepia(50%) saturate(423%) hue-rotate(105deg) brightness(86%) contrast(93%);';
      }
    }};
  }

  @media (max-width: 900px) {
    padding-top: 8vh;
  }
  @media (min-width: 900px) {
    padding: 20px 0;
    display: flex;
    height: calc(100vh - 40px);
    justify-content: space-between;
    -webkit-justify-content: space-between;

    .home {
      border-left: 0px;
    }
  }
`;
