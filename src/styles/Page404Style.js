import styled from 'styled-components';

const Page404Style = styled.div`
  .content {
    height: 58vh;
    padding-top: 10vh;
  }

  img {
    width: 7rem;
    opacity: 1;
    margin-top: -45px;
  }

  @media (min-width: 900px) {
    h3 {
      font-size: 7rem;
      line-height: 8rem;
    }
  }
`;

export default Page404Style;
