import styled from 'styled-components';

const FormStyle = styled.div`
  margin-top: 20px;
  h3 {
    padding-bottom: 20px;
  }

  button {
    cursor: pointer;
    display: flex;
    padding: 40px 0;
    padding-bottom: 20px;
    border: 0;
    color: var(--font-color) !important;
    background-color: var(--background-color) !important;
    border-top: 1px solid var(--font-color) !important;
    width: 100%;
    text-align: center;
    justify-content: center;
    -webkit-justify-content: center;
    opacity: 1;
    font-family: founderNormal, -apple-system, BlinkMacSystemFont, 'Segoe UI',
      Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue',
      sans-serif;
    text-transform: uppercase;
    font-size: 2rem;
    &:hover {
      opacity: 0.5;
    }
  }

  input {
    padding: 10px 0;
    width: 100%;
    text-align: center;
    color: var(--font-color);
    font-size: 3.5rem;
    flex-grow: 1;
    border: 0px;
    background-color: var(--background-color);
    border-top: 1px solid var(--font-color);
  }

  input:focus-visible {
    border: 0 !important;
    outline: none;
    border-top: 1px solid var(--font-color) !important;
  }

  input:focus {
    border: 0 !important;
    outline: none;
    border-top: 1px solid var(--font-color) !important;
  }

  @media (min-width: 900px) {
    button {
      padding: 40px 0;
    }
    input {
      padding: 20px 0;

      font-size: 5rem;
    }
  }
`;

export default FormStyle;
