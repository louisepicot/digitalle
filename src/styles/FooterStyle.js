import styled from 'styled-components';

const FooterStyle = styled.div`
  height: auto !important;
  text-align: center;
  margin-top: 70px;
  .row1,
  .row2 {
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    padding-top: 0;
  }

  .item {
    /* text-transform: uppercase; */
    margin-top: 20px;
    div {
      display: flex;
      padding: 0px;
      align-items: center;
      justify-content: center;

      h2 {
        margin-top: 0 !important;
      }
      a {
        width: 5%;
        margin-top: 10px;
        margin-left: 10px;
      }
    }
    .icon {
      list-style-type: none;

      margin-top: 0px;
      margin-left: 0px;
    }
    .logo {
      font-family: RecoletaMedium;
      line-height: 4rem;
      font-size: 4rem;
    }
    h4,
    p,
    a {
      font-size: 1.3rem;
      line-height: 1rem;
    }
  }

  h4 {
    font-family: founderNormal;
  }

  p {
    font-family: recoletaAlt;
  }

  @media (min-width: 900px) {
    text-align: left;
    padding-top: 0rem;
    padding-bottom: 20px;
    border-top: 1px solid;
    .row1,
    .row2 {
      flex-direction: row;
      align-items: flex-end;
      padding-top: 20px;
    }
    .item {
      width: calc((100% / 3) - 50px);
    }
    div {
      padding-left: 20px;
      margin-bottom: 0;
      justify-content: flex-start !important;
      /* display: flex;
      justify-content: center;  */
    }
  }
`;

export default FooterStyle;
