import { createGlobalStyle } from 'styled-components';

import recoletaWoff from '../assets/fonts/Recoleta Medium/Recoleta-Medium.woff';
import recoletaEot from '../assets/fonts/Recoleta Medium/Recoleta-Medium.eot';
import recoletaTtf from '../assets/fonts/Recoleta Medium/Recoleta-Medium.ttf';
import recoletaAlt from '../assets/fonts/RecoletaAltMedium/font.woff';
import founderNormal from '../assets/fonts/Founders Grotesk Web Font/founders-grotesk-light.woff2';
import founderItalic from '../assets/fonts/Founders Grotesk Web Font/founders-grotesk-light-italic.woff2';
import doodle from '../assets/fonts/DigitalleDoodles-Regular.ttf';

const Typography = createGlobalStyle`


  @font-face {
    font-family: RecoletaMedium;
    src: url(${recoletaEot}); /* IE9 Compat Modes */
    src: url(${recoletaEot}?#iefix) format('embedded-opentype'), /* IE6-IE8 */
         url(${recoletaWoff}) format('woff'), /* Modern Browsers */
         url(${recoletaTtf}) format('truetype'); /* Safari, Android, iOS */
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}

@font-face {
    font-family: recoletaAlt;
    src: url(${recoletaAlt}) format('woff2'); /* Modern Browsers */
}

@font-face {
    font-family: founderNormal;
    src: url(${founderNormal}) format('woff2'); /* Modern Browsers */
}

@font-face {
    font-family: doodle;
    src: url(${doodle}) format('woff'); /* Modern Browsers */
}


  html {
    font-family: RecoletaMedium, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    color: var(--black);
    line-height: 2.5rem;
  }


  h1,h2,h3,h4,h5,h6 {
    font-weight: normal;
    margin: 0;
    font-family: recoletaAlt, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
  }

  p {
    font-family: founderNormal, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-size: 2.5rem;
      line-height: 3rem;
      margin-top: 2rem;
  }

  li{
    font-family: founderNormal, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    list-style-position: inside;
    margin-top: 2rem;
    font-size: 2.5rem;
line-height: 3rem;
}

  ul {
    padding-left: 20px;
    margin-bottom: 0px;
    margin-top: 2rem;
  }
  h2, h3 {
    font-size: 3rem;
    line-height: 3.5rem;
    margin-top: 10px;
  }

  h4{
    font-size: 1.8rem;
    line-height: 2.5rem;
    margin-top: 2rem;
  }

  h5 {
    font-family: founderNormal, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    text-transform: uppercase;
    font-size: 2rem;
  }


  @media (min-width: 900px) {


    h2 {
        font-size: 7.5rem;
        line-height: 8rem;
      }

      h3 {
    font-size: 5rem;
    line-height: 6rem;
  }

    h4{
        font-size: 2.5rem;
        line-height: 3.5rem;

      }

    h5{
        font-size: 2.5rem;
        line-height: 3.5rem;
    }

  }


  @media (max-width: 900px) {
    p {
    font-family: founderNormal, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-size: 2.5rem;
      line-height: 3rem;
      margin-top: 2rem;
  }

  li{
    font-family: founderNormal, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    list-style-position: inside;
    margin-top: 2rem;
    font-size: 2.5rem;
  line-height: 3rem;
  }

  ul {
    padding-left: 20px;
    margin-bottom: 0px;
    margin-top: 2rem;
  }
  h2, h3 {
    font-size: 3.5rem;
    line-height: 4rem;
    margin-top: 10px;
  }

  h4{
    font-size: 2.5rem;
    line-height: 3rem;
    margin-top: 2rem;
  }

  h5 {
    font-family: founderNormal, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    text-transform: uppercase;
    font-size: 2.5rem;
  }


}
`;

export default Typography;
