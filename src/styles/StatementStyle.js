import styled from 'styled-components';

const StatementStyle = styled.div`
  img {
    width: 7rem;
    opacity: 1;
    margin-top: -45px;
  }

  @media (min-width: 900px) {
    h3 {
      font-size: 7rem;
      line-height: 8rem;
    }

    img {
      width: 7%;
      opacity: 0;
      margin-top: -850px;
    }
  }
`;

export default StatementStyle;
