import styled from 'styled-components';

const ContentPageStyle = styled.div`
  position: relative;
  overflow-y: scroll;
  height: 70vh;
  width: auto;

  .snap-scroll {
    margin: 20px;
    border-bottom: 1px solid;
    margin-top: 60px;
    padding-bottom: 20px;
    :last-child {
      padding-bottom: 0px;
      border-bottom: 0;
    }
    .newsletter {
      padding-bottom: 0px !important;
    }
  }

  .newsletter {
    display: flex;
    align-items: flex-end;
    -webkit-align-items: flex-end;
  }

  .gatsby-image-wrapper {
    object-fit: contain;
  }

  @media (min-width: 900px) {
    .snap-scroll {
      margin: 0;
      border-bottom: 0;
      /* height: 100vh; */
      :last-child {
        margin-top: 8rem;
        padding-bottom: 20px;
      }
    }
    padding: 0px 10px 0px 10px;
    height: calc(70vh - 20px);

    .modul2-snap {
      min-height: 300vh;
    }

    .newsletter {
      height: 100vh;
    }

    padding-bottom: 20px;
    .intro {
      padding-top: -40px !important;
    }

    .gatsby-image-wrapper {
      object-fit: contain;
    }
    padding: 0px 50px 0px 50px;
    width: 100%;
    height: calc(100vh - 20px);

    /* .modul2-snap.snap-scroll {
      scroll-snap-align: ${(props) => (props.goingUp ? 'end' : 'start')};
    } */
  }

  @media (max-width: 900px) {
    height: 90vh;
  }
`;

export default ContentPageStyle;
