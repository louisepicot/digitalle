import React from 'react';
import { graphql } from 'gatsby';

import ContentPage from '../components/ContentPage';

export default function About({ data }) {
  const dataAbout = data.allSanityAbout.nodes;
  const sortedNodes = [];

  dataAbout.forEach((node) => {
    if (node.category === 'Intro') {
      sortedNodes.push(node);
    }
  });

  dataAbout.forEach((node) => {
    if (node.category === 'Manifesto') {
      sortedNodes.push(node);
    }
  });

  dataAbout.forEach((node) => {
    if (node.category === 'The digitalle experience') {
      sortedNodes.push(node);
    }
  });

  return (
    <>
      <ContentPage
        titles={[
          'Manifesto',
          'The digitalle experience',
          'Newsletter',
        ].reverse()}
        data={sortedNodes}
        page="About"
      />
    </>
  );
}

export const query = graphql`
  query {
    allSanityAbout(sort: { fields: category }) {
      nodes {
        id
        category
        intro {
          illu {
            asset {
              fluid(maxWidth: 10) {
                ...GatsbySanityImageFluid
              }
            }
          }
          _rawText(resolveReferences: { maxDepth: 10 })
        }
        moduleType {
          module1 {
            _rawText(resolveReferences: { maxDepth: 10 })
            title
            illu {
              asset {
                fluid(maxWidth: 10) {
                  ...GatsbySanityImageFluid
                }
              }
            }
          }
          module2 {
            tiroir {
              _key
              _rawText(resolveReferences: { maxDepth: 10 })
              illu {
                asset {
                  fluid(maxWidth: 10) {
                    ...GatsbySanityImageFluid
                  }
                }
              }
            }
          }
          moduleType
        }
      }
    }
    seo: sanitySeo(title: { eq: "About" }) {
      id
      image {
        asset {
          fluid(maxWidth: 700) {
            ...GatsbySanityImageFluid
          }
        }
      }
      title
      titlePage
      description
    }
  }
`;
