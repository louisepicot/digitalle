import React from 'react';
import { graphql } from 'gatsby';

import ContentPage from '../components/ContentPage';

export default function Workshops({ data }) {
  const dataWorkshops = data.allSanityWorkshops.nodes;
  const sortedNodes = [];

  dataWorkshops.forEach((node) => {
    if (node.category === 'Intro') {
      sortedNodes.push(node);
    }
  });

  dataWorkshops.forEach((node) => {
    if (node.category === 'Digital collaboration') {
      sortedNodes.push(node);
    }
  });

  dataWorkshops.forEach((node) => {
    if (node.category === 'Retrospective') {
      sortedNodes.push(node);
    }
  });
  dataWorkshops.forEach((node) => {
    if (node.category === 'Customs workshops') {
      sortedNodes.push(node);
    }
  });

  return (
    <>
      <ContentPage
        titles={[
          'Digital collaboration',
          'Retrospective',
          'Customs workshops',
        ].reverse()}
        data={sortedNodes}
        page="Workshops"
      />
    </>
  );
}

export const query = graphql`
  query {
    allSanityWorkshops(sort: { fields: category }) {
      nodes {
        id
        category
        intro {
          illu {
            asset {
              fluid(maxWidth: 10) {
                ...GatsbySanityImageFluid
              }
            }
          }
          _rawText(resolveReferences: { maxDepth: 10 })
        }
        moduleType {
          module1 {
            _rawText(resolveReferences: { maxDepth: 10 })
            title
            illu {
              asset {
                fluid(maxWidth: 10) {
                  ...GatsbySanityImageFluid
                }
              }
            }
          }
          module2 {
            tiroir {
              _key
              _rawText(resolveReferences: { maxDepth: 10 })
              illu {
                asset {
                  fluid(maxWidth: 10) {
                    ...GatsbySanityImageFluid
                  }
                }
              }
            }
          }
          moduleType
        }
      }
    }
    seo: sanitySeo(title: { eq: "Workshops" }) {
      id
      image {
        asset {
          fluid(maxWidth: 700) {
            ...GatsbySanityImageFluid
          }
        }
      }
      title
      titlePage
      description
    }
  }
`;
