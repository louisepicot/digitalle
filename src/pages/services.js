import React from 'react';
import { graphql } from 'gatsby';
import ContentPage from '../components/ContentPage';

export default function Services({ data }) {
  const dataServices = data.allSanityServices.nodes;
  const sortedNodes = [];

  dataServices.forEach((node) => {
    if (node.category === 'Intro') {
      sortedNodes.push(node);
    }
  });

  dataServices.forEach((node) => {
    if (node.category === 'Digital strategy') {
      sortedNodes.push(node);
    }
  });

  dataServices.forEach((node) => {
    if (node.category === 'Digital marketing') {
      sortedNodes.push(node);
    }
  });
  dataServices.forEach((node) => {
    if (node.category === 'Content strategy') {
      sortedNodes.push(node);
    }
  });

  dataServices.forEach((node) => {
    if (node.category === 'Digitalle empowerment') {
      sortedNodes.push(node);
    }
  });

  return (
    <>
      <ContentPage
        titles={[
          'Digital strategy',
          'Digital marketing',
          'Content strategy',
          'Digitalle empowerment',
        ].reverse()}
        data={sortedNodes}
        page="Services"
      />
    </>
  );
}

export const query = graphql`
  query {
    allSanityServices(sort: { fields: category }) {
      nodes {
        id
        category
        intro {
          illu {
            asset {
              fluid(maxWidth: 10) {
                ...GatsbySanityImageFluid
              }
            }
          }
          _rawText(resolveReferences: { maxDepth: 10 })
        }
        moduleType {
          module1 {
            _rawText(resolveReferences: { maxDepth: 10 })
            title
            illu {
              asset {
                fluid(maxWidth: 10) {
                  ...GatsbySanityImageFluid
                }
              }
            }
          }
          module2 {
            tiroir {
              _key
              _rawText(resolveReferences: { maxDepth: 10 })
              illu {
                asset {
                  fluid(maxWidth: 10) {
                    ...GatsbySanityImageFluid
                  }
                }
              }
            }
          }
          moduleType
        }
      }
    }
    seo: sanitySeo(title: { eq: "Services" }) {
      id
      image {
        asset {
          fluid(maxWidth: 700) {
            ...GatsbySanityImageFluid
          }
        }
      }
      title
      titlePage
      description
    }
  }
`;
