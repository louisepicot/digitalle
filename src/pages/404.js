import { graphql } from 'gatsby';
import React from 'react';
import Footer from '../components/Footer';
import HomePageStyle from '../styles/HomePageStyle';

export default function FourOhFourPage({ data }) {
  return (
    <HomePageStyle>
      "hey"
      <Footer />
    </HomePageStyle>
  );
}
export const query = graphql`
  query {
    seo: sanitySeo(title: { eq: "Homepage" }) {
      id
      image {
        asset {
          fluid(maxWidth: 700) {
            ...GatsbySanityImageFluid
          }
        }
      }
      title
      titlePage
      description
    }
  }
`;
