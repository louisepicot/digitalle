import React, { useEffect, useState } from 'react';

import { polyfill } from 'seamless-scroll-polyfill';
import { graphql } from 'gatsby';
import Footer from '../components/Footer';
import Statement from '../components/Statement';
import HomePageStyle from '../styles/HomePageStyle';

export default function HomePage({ data }) {
  const items = [];

  for (let i = 0; i <= 7; i++) {
    items.push(<Statement key={`home${i}`} index={i} />);
  }

  const [isDesktop, setDesktop] = useState(false);
  const updateMedia = () => {
    setDesktop(window.innerWidth > 768);
  };

  useEffect(() => {
    if (typeof window !== 'undefined') {
      setDesktop(window.innerWidth > 768);
    }
  }, []);

  useEffect(() => {
    polyfill();
    let count = 1;
    setInterval(() => {
      if (count <= 7) {
        if (document.querySelector(`.home${count}`)) {
          document.querySelector(`.home${count}`).scrollIntoView({
            behavior: 'smooth',
            block: 'start',
          });
          count++;
        }
      }
    }, 1000);
    window.addEventListener('resize', updateMedia);
    return () => window.removeEventListener('resize', updateMedia);
  }, [isDesktop]);

  return (
    <>
      <HomePageStyle>
        <div className="content">{items}</div>
        <Footer />
      </HomePageStyle>
    </>
  );
}

export const query = graphql`
  query {
    seo: sanitySeo(title: { eq: "Homepage" }) {
      id
      image {
        asset {
          fluid(maxWidth: 700) {
            ...GatsbySanityImageFluid
          }
        }
      }
      title
      titlePage
      description
    }
  }
`;
